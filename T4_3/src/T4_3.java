/**
 * 
 */

/**
 * @author francis
 *
 */
public class T4_3 {

	/**
	 Escribe un programa Java que realice lo siguiente: declarar dos variables X e Y de tipo int,
	dos variables N y M de tipo double y asigna a cada una un valor. A continuaciσn muestra por
	pantalla:
	 El valor de cada variable.
	 La suma X + Y
	 La diferencia X  Y
	 El producto X * Y
	 El cociente X / Y
	 El resto X % Y
	 La suma N + M
	 La diferencia N  M
	 El producto N * M
	 El cociente N / M
	 El resto N % M
	
	 La suma X + N
	 El cociente Y / M
	 El resto Y % M
	 El doble de cada variable
	 La suma de todas las variables
	 El producto de todas las variables
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int X = 1, Y = 2;
		double N = 2.5, M = 3.5;
		System.out.println("Valor de X = " + X);
		System.out.println("Valor de Y = " + Y);
		System.out.println("Valor de N = " + N);
		System.out.println("Valor de M = " + M);
		
		System.out.println("Suma de X + Y = " + (X + Y));
		System.out.println("Diferencia de X - Y = " + (X - Y));
		System.out.println("Producto de X * Y = " + (X * Y));
		System.out.println("Cociente de X / Y = " + (X / Y));
		System.out.println("Resto de X % Y = " + (X % Y));
		
		System.out.println("Suma de N + M = " + (N + M));
		System.out.println("Diferencia de N - M = " + (N - M));
		System.out.println("Producto de N * M = " + (N * M));
		System.out.println("Cociente de N / M = " + (N / M));
		System.out.println("Resto de N % M = " + (N % M));
		
		System.out.println("Suma de X + N = " + (X + N));
		System.out.println("Cociente de Y / M = " + (Y / M));
		System.out.println("Resto de Y % M = " + (Y % M));

		System.out.println("Valor de X*2 = " + (X*2));
		System.out.println("Valor de Y*2 = " + (Y*2));
		System.out.println("Valor de N*2 = " + (N*2));
		System.out.println("Valor de M*2 = " + (M*2));
		System.out.println("Suma total = " + (X + Y + N + M));
		System.out.println("Producto = " + (X * Y * N * M));
		System.out.println("Proceso finalizado");
	}

}
